from django.shortcuts import render

from vehicles.models import Vehicle

#http://127.0.0.1:8000/vehicles
def get_vehicles(request):
    #select * from vehicles_vehicle
    vehicles = Vehicle.objects.all()
    return render (request, "vehicle_list.html", context={
        "vehicles": vehicles
    })


#http://127.0.0.1:8000/vehicles/2
def get_vehicle_details(request, pk):
    # select * from vehicles_vehicle where id= %s
    vehicle = Vehicle.objects.get(pk=pk)
    return render(request, "vehicle_detail.html", context={
        "vehicle": vehicle
    })

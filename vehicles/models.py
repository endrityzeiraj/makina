from django.db import models

# Create your models here, create TABLE vehicles()
class Vehicle(models.Model):
    AUTOMATIC = "automatic"
    MANUAL = "manual"
    TRANSMISSION_CHOICES = [
        (AUTOMATIC, "Automatic"),
        (MANUAL, "Manual")
    ]

    brand = models.CharField(max_length=100, null=False)
    #same as in SQL: brand varchar(100) not null 
    model = models.CharField(max_length=100, null=False)
    #same as in SQL: model varchar(100) not null
    year = models.PositiveBigIntegerField(null=False)
    #year int not null constraint chk_positive check (year>0)
    transmission = models.CharField(max_length=32, null=True, choices=TRANSMISSION_CHOICES)
    photo = models.ImageField(null= True, upload_to="vehicles")



    def __str__(self):
        return f'{self.brand} {self.model}'

